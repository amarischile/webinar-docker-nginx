# NGINX GATEWAY

Contenedor creado para actuar como gateway de los ms del webinar.
Se basa en la imagen oficial `nginx:alpine` 

## Container Configuration

### PORT FORWARDING

El contenedor se ejecuta en el puerto `80` ya que es la puerta de entrada a los microservicios.

## Gateway Configuration

Para esto el `Dockerfile` copia un archivo de configuracion que se encuentra en la misma carpeta el cual es `default.conf` dentro de este van todas las configuracion asociadas al servidor web en caso de querer modificar revisar documentacion oficial [AQUI](https://www.nginx.com/resources/wiki/start/topics/examples/full/)

### default.conf

Dentro del archivo va la configuracion del servidor y dentro de ella el mapeo a los respectivos puertos.

Se ejecuta el servidor web en puerto `80` para `ipv4` e `ipv6` el `servername` queda a la escucha del `hostname` `0.0.0.0` y se activan la configuraciones de compresion `gzip` además se incluyen las configuraciones que puedan estar en la carpeta `/etc/nginx/default.d/*.conf`la cual no se añaden por defecto.
```sh
server {
    listen       80;
    listen       [::]:80;
    server_name  0.0.0.0;
    root         /usr/share/nginx/html;

    # Compression Gzip
    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 9;
    gzip_buffers 32 16k;
    gzip_http_version 1.1;
    gzip_min_length 1100;
    gzip_types text/plain text/css application/x-javascript application/json text/javascript;
```

Regla utilizada para los distintos `proxys` a las aplicaciones, expresion regular que toma todo lo que lleve el `path` `/webinar-docker/api/covid-19` luego la reescribe y toma todos los parametros extras de ese `path` para luego hacer el proxy a la aplicacion la cual utiliza como `hostname` el nombre del contenedor.

```sh
   location ^~ /webinar-docker/api/covid-19 {
        # Micro caching
        proxy_cache microcacheapi;
        proxy_cache_valid 200 1s;
        proxy_cache_use_stale updating;
        proxy_cache_background_update on;
        proxy_cache_lock on;

        resolver 127.0.0.1;

        rewrite ^/webinar-docker/api/covid19/?(.*) /$1 break;
        proxy_pass http://ms-webinar-docker-covid-19:8081;
        proxy_redirect off;
    }
```

## Container Execution

Para la ejecucion solo hace falta tener instalado Docker dentro de la maquina y en una consola ejecutar lo siguiente.

Por temas de conectividad este contenedor necesita todos los contenedores que esten estén mapeados en el archivo `default.conf`

```sh
# En la carpeta del proyecto.

docker build -t webinar-docker/nginx .
```
Con esto crearemos la imagen llamada `webinar-docker/nginx` y con el `.` le decimos que contruya en base al `Dockerfile` del directorio.

Luego ejecutamos lo siguiente para ejecutar el `webinar-docker/nginx`.

```sh
docker run --rm --network webinar-docker --name webinar-docker-nginx -p 80:80 webinar-docker/nginx
```

Esto ejecutará el contenedor de Docker en el puerto `80` y le añade multiples variables de entorno y configuraciones.
En caso de querer ejecutar la imagen en background agregar luego del `-p 80:80` un `-d` o `--detached`.